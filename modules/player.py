#!/usr/bin/python3.4

class Player:
    def __init__(self):
        self.player_cards = []
        self.player_points = 0

    def add_card(self, card):
        self.player_cards.append(card)
        self.player_points += card.value()

    def anon(self):
        print('Bot hand: ' + '[***] '*len(self.player_cards))

    def status(self):
        view_hand = ''
        for card in self.player_cards:
            view_hand += card.view()
        print('Hand: {1} Total: {0}'.format(self.player_points, view_hand))

    def check(self, user):
        if self.player_points == 21:
            print(user + ' WIN WITH:')
            self.status()
            return True
        elif self.player_points > 21:
            print(user + ' LOOSE WITH:')
            self.status()
            return True
        else:
            return False
