#!/usr/bin/python3.4
class ValDataException(Exception):
    def __init__(self, dErrArguments):
        Exception.__init__(self, "Incorrect value or datatype for arguments: {0}".format(dErrArguments))
        self.dErrorArguments = dErrArguments
