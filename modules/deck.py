#!/usr/bin/python3.4
from random import shuffle
from .card import Card

class Deck:

    def form_deck(self):
        deck = []
        for suit_id in range(0, 4):
            for number in range(1, 14):
                deck.append(Card(suit_id, number))
        return deck

    def __init__(self):
        self.deck = self.form_deck()
        shuffle(self.deck)

    def pull(self):
        card = self.deck.pop(0)
        return card

    def remains(self):
        return len(self.deck)
