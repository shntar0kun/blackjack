from .exceptions import ValDataException

class Card:

    SUITS = ('<3', '<3<', '<>', '>+')

    def check_data(self, value, start, end):
        if value in range(start, end) and isinstance(value, int):
            return True
        else:
            raise ValDataException({
                'value': value,
                'start': start,
                'end': end
            })

    def __init__(self, suit_id, number):

        if self.check_data(suit_id, 0, 5):
            self.suit = self.SUITS[suit_id]

        if self.check_data(number, 1, 14):
            self.number = number

    def value(self):
        if self.number in range(2, 11):
            return self.number
        elif self.number in range(11, 14):
            return 10
        else: return 1

    def view(self):
        view_alt = {11: 'J', 12: 'Q', 13: 'K'}
        if self.number in range(11, 14):
            return '[{0} {1}] '.format(view_alt[self.number], self.suit)
        else:
            return '[{0} {1}] '.format(self.number, self.suit)