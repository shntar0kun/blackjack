#!/usr/bin/python3.4
import unittest

from modules.player import Player
from modules.card import Card
class TestStringMethods(unittest.TestCase):

    def test_bad_add_card(self):
        player = Player()
        with self.assertRaises(AttributeError):
            player.add_card(1)
            player.add_card('adsfasdf')
            player.add_card(True)

    def test_good_add_card(self):
        player = Player()
        card = Card(0, 1)
        player.add_card(card)
        self.assertEqual(card.value(), player.player_points)
        self.assertIsInstance(player.player_cards[0], Card)

    def test_check(self):
        player = Player()
        player.player_points = 21
        self.assertTrue(player.check('Vasya'))
        player.player_points = 22
        self.assertTrue(player.check('Vasya'))
        player.player_points = 10
        self.assertFalse(player.check('Vasya'))

if __name__ == '__main__':
    unittest.main()