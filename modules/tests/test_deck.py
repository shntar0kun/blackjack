#!/usr/bin/python3.4
import unittest

from modules.deck import Deck
from modules.card import Card
class TestStringMethods(unittest.TestCase):
    deck = Deck()
    card = Card(0, 1)

    def test_card_instance(self):
        self.assertIsInstance(self.deck.pull(), Card)

    def test_leftovers(self):
        self.assertEqual(len(self.deck.deck), self.deck.remains())

if __name__ == '__main__':
    unittest.main()