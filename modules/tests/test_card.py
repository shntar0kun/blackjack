#!/usr/bin/python3.4
import unittest

from modules.card import Card
from modules.exceptions import ValDataException
class TestStringMethods(unittest.TestCase):

    def test_good_creation(self):
        card = Card(0, 1)
        self.assertEqual(card.suit, '<3')
        self.assertEqual(card.number, 1)

    def test_bad_creation(self):
        with self.assertRaises(ValDataException):
            Card(-1, 20)

if __name__ == '__main__':
    unittest.main()