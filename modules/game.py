#!/usr/bin/python3.4
from .deck import Deck
from .player import Player

class Game:
    def __init__(self):
        self.deck = Deck()
        self.player = Player()
        self.bot = Player()

    def step(self, username, player, is_bot = False):
        if is_bot:
            player.add_card(self.deck.pull())
            player.anon()
        else:
            player.status()
            while True:
                result = int(input("Your turn. Please enter '1' to pull card or '0' to pass: "))
                if result in (0,1):
                    break
            if result == 1:
               player.add_card(self.deck.pull())
        return player.check(username)

    def start(self):
        print('GAME STARTED')
        self.bot.add_card(self.deck.pull())
        self.player.add_card(self.deck.pull())
        print('***********************************')
        self.bot.anon()
        self.player.status()
        while True:
            print('***********************************')
            if self.step('BOT', self.bot, True):
                break
            elif self.step('USER ', self.player):
                break
            print('***********************************')
